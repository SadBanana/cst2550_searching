import java.util.Iterator;

public class Queue<T> implements Iterable<T>{
    
    Node front = null;
    Node back = null;

    private class Node{
	T item;
	Node next;

	public Node(T item, Node next){
	    this.item = item;
	    this.next = next;
	}
    }

    
    public void enqueue(T item){
	
	Node temp = back;
	back = new Node(item, null);
	
	if(isEmpty())
	    front = back;
	else
	    temp.next = back;
    }
    
    public T dequeue(){
	T item = front.item;
	front = front.next;
	return item;
    }
    
    public boolean isEmpty(){
	return front == null;
    }

    public Iterator<T> iterator(){
	return new QueueIterator();
    }

    private class QueueIterator implements Iterator<T>{
	private Node current = front;

	public boolean hasNext(){
	    return current != null;
	}

	public T next(){
	    T item = current.item;
	    current = current.next;
	    return item;
	}
    }
}
